import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array

def print_results(model_path, image):
    model = load_model(model_path)

    # Load and resize the image
    img = load_img(image, target_size=(512, 512))
    image_arr = img_to_array(img) / 255.0

    # Predict the mask
    image_arr = np.expand_dims(image_arr, axis=0)
    output = model.predict(image_arr).reshape(512, 512)

    # Create subplots
    fig, ax = plt.subplots(1, 2, figsize=(15, 10))  # Two subplots side by side

    # Display the input image
    ax[0].imshow(img)
    ax[0].set_title("Input Image")

    # Display the predicted mask
    ax[1].imshow(output, cmap='gray')
    ax[1].set_title("Predicted Mask")

    plt.show()

# Example usage
print_results('model_unet.h5', 'WhatsApp Image 2024-08-12 at 19.58.20_54d35522-min.jpg')

