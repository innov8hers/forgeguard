# Forge Guard: Digital Image Forgery Detection Tool

Forge Guard is a tool designed to address the growing issue of digital image forgery. This application allows users to upload images on a web-based interface and verifies whether an image is authentic or forged. The project is based on the findings of the research paper: [Detection of Image Forgery Using Deep Learning](https://ieeexplore.ieee.org/document/9678207).

## Features
- **Upload Image**: Users can upload images through the web interface.
- **Forgery Detection**: The tool analyzes the uploaded image for potential signs of forgery.
- **Visualization**: Highlights the areas of the image that are detected as forged.

## Installation and Setup

### Prerequisites
Ensure you have the following packages installed:

- **TensorFlow**
- **NumPy**
- **Pillow (PIL)**
- **Flask**
- **os** (included in Python)

1. You can install these packages using the following commands:

    ```bash
    pip install tensorflow
    pip install numpy
    pip install pillow
    pip install flask

## Running the Application
1. Extract the zip file from the repository.
2. Navigate to the project directory.
3. Run the following command to start the application:

    ```bash
    python new.py
    ```

4. Open your web browser and navigate to [http://localhost:5000](http://localhost:5000) to use the application.

## How It Works
Forge Guard uses deep learning techniques for noise analysis and Error Level Analysis (ELA) to detect discrepancies in the image that may indicate tampering. This analysis is presented to the user, highlighting any potential forged areas.
