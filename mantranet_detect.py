#%load_ext autoreload 
#%autoreload 2

#import os

#os.chdir('./ManTraNet-pytorch/MantraNet')

#import matplotlib.pyplot as plt
#import gc

#from mantranet import *

#from pytorch_lightning import Trainer


#model=pre_trained_model(weight_path='./MantraNetv4.pt')

#os.chdir(os.path.dirname(os.path.dirname(os.getcwd())))

#plt.figure(figsize=(20,20))
#check_forgery(model,img_path='image/highlighted_image.jpg')

import os
import importlib
import matplotlib.pyplot as plt
import gc
import sys
# Change directory to the required path
os.chdir('./ManTraNet-pytorch/MantraNet')
print(os.getcwd())

sys.path.append(os.getcwd())
# Import the module you need
from mantranet import *

# Optionally, reload the mantranet module to ensure the latest version is used
#importlib.reload(importlib.import_module('mantranet'))

# Import other necessary modules
from pytorch_lightning import Trainer

# Initialize the model with pre-trained weights
model = pre_trained_model(weight_path='./MantraNetv4.pt')

# Change directory back to the parent directory
os.chdir(os.path.dirname(os.path.dirname(os.getcwd())))

# Set up the plot
plt.figure(figsize=(20, 20))

# Check forgery on the given image
check_forgery(model, img_path='image/highlighted_image.jpg')

