import os
from flask import Flask, request, render_template, redirect, url_for, flash
from tensorflow.keras.models import load_model
import numpy as np
from PIL import Image, ImageChops, ImageEnhance

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'uploads'
app.secret_key = 'ForgeGuard'

# Load the model
model = load_model('model.h5')

def convert_to_ela_image(path, quality):
    temp_filename = 'temp_file_name.jpg'
    ela_filename = 'temp_ela.png'

    image = Image.open(path).convert('RGB')
    image.save(temp_filename, 'JPEG', quality=quality)

    saved = Image.open(temp_filename)
    ela_image = ImageChops.difference(image, saved)

    extrema = ela_image.getextrema()
    max_diff = max([ex[1] for ex in extrema])
    scale = 255.0 / max_diff
    ela_image = ImageEnhance.Brightness(ela_image).enhance(scale)

    return ela_image

def prepare_image(image_path):
    ela_image = convert_to_ela_image(image_path, 90)
    resized_ela_image = ela_image.resize([128, 128])
    image_array = np.array(resized_ela_image) / 255.0  # Normalize to [0,1]
    return image_array

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/send_message', methods=['POST'])
def send_message():
    name = request.form.get('name')
    email = request.form.get('email')
    message = request.form.get('message')

    # Here you would handle the form submission, e.g., send an email or store in a database
    # For now, we'll just flash a success message and redirect to the contact page
    flash('Message sent successfully!', 'success')
    return redirect(url_for('contact'))

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)

        file = request.files['file']
        if file.filename == '':
            return redirect(request.url)

        if file:
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
            file.save(file_path)

            preprocessed_img = prepare_image(file_path)
            preprocessed_img = np.expand_dims(preprocessed_img, axis=0)  # Add batch dimension

            prediction = model.predict(preprocessed_img)
            is_forged = np.argmax(prediction, axis=1)[0]

            result = "Forged" if is_forged == 1 else "Not Forged"
            return render_template('uploadImage.html', result=result)
    else:
        return render_template('uploadImage.html')

if __name__ == '__main__':
    if not os.path.exists(app.config['UPLOAD_FOLDER']):
        os.makedirs(app.config['UPLOAD_FOLDER'])
    app.run(debug=True)

