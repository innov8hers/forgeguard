import numpy as np
import numpy as np
import cv2
from matplotlib import pyplot as plt


kernel = np.array([
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0,-1 ,1, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0,-1 ,0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 1,-1 ,0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0,-1 ,0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0],
            [0, 0,-1 ,0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0],
            [0, 0,-1 ,0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0,-1 ,0, 0],
            [0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0,-1 ,0, 0],
            [0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 1,-2 ,1, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0,-2 ,0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0],
            [0, 0,-2 ,0, 0],
            [0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0],
            [0, 0,-2 ,0, 0],
            [0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 1,-3 ,3,-1],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0,-1, 0, 0],
            [0, 0, 3, 0, 0],
            [0, 0,-3 ,0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [-1,3,-3 ,1, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0,-3 ,0, 0],
            [0, 0, 3, 0, 0],
            [0, 0,-1, 0, 0],
        ],
        [
            [0, 0, 0, 0,-1],
            [0, 0, 0, 3, 0],
            [0, 0,-3 ,0, 0],
            [0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [-1,0, 0, 0, 0],
            [0, 3, 0, 0, 0],
            [0, 0,-3 ,0, 0],
            [0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0],
            [0, 0,-3 ,0, 0],
            [0, 3, 0, 0, 0],
            [-1,0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0],
            [0, 0,-3, 0, 0],
            [0, 0, 0, 3, 0],
            [0, 0, 0, 0,-1],
        ],
        [
            [0, 0, 0, 0, 0],
            [0,-1, 2,-1, 0],
            [0, 2,-4, 2, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0,-1, 2, 0, 0],
            [0, 2,-4 ,0, 0],
            [0,-1, 2, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 2,-4, 2, 0],
            [0,-1, 2,-1, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 2,-1, 0],
            [0, 0,-4 ,2, 0],
            [0, 0, 2,-1, 0],
            [0, 0, 0, 0, 0],
        ],

        [
            [0, 0, 0, 0, 0],
            [0,-1, 2,-1, 0],
            [0, 2,-4 ,2, 0],
            [0,-1, 2,-1, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [-1,2,-2, 2,-1],
            [2,-6, 8,-6, 2],
            [-2,8,-12,8,-2],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ],
        [
            [-1, 2,-2, 0, 0],
            [ 2,-6, 8, 0, 0],
            [-2,8,-12 ,1, 0],
            [ 2,-6, 8, 0, 0],
            [-1, 2,-2, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [-2,8,-12,8,-2],
            [2,-6, 8,-6, 2],
            [-1,2,-2, 2,-1],
        ],
        [
            [0,0,-2, 2, -1],
            [0,0, 8, -6, 2],
            [0,0,-12 ,8, -2],
            [0,0, 8, -6, 2],
            [0,0,-2, 2,-1],
        ],
        [
            [-1,2,-2, 2, -1],
            [2,-6, 8, -6, 2],
            [-2,8,-12 ,8, -2],
            [2,-6, 8, -6, 2],
            [-1,2,-2, 2,-1],
        ]])
import numpy as np
import cv2
from matplotlib import pyplot as plt

# Define the 30 SRM filters
filters = kernel

def convert_to_3_channel_filters(filters):
    if filters.ndim == 3:
        filters_3_channel = np.stack([filters, filters, filters], axis=-1)
        return filters_3_channel
    else:
        raise ValueError("Input filters must be a 3D NumPy array (num_filters, height, width).")

# Convert to 3-channel filters
filters_3_channel = convert_to_3_channel_filters(filters)

def apply_filters(image, filters, amplification_factor=1):
    noise_maps = []
    for f in filters:
        noise_map_channels = []
        for c in range(3):  # Apply filter to each channel
            noise_map = cv2.filter2D(image[:, :, c], -1, f[:, :, c]) * amplification_factor
            noise_map_channels.append(noise_map)
        noise_map = np.stack(noise_map_channels, axis=-1)  # Combine the three channels
        noise_maps.append(noise_map)
    return np.array(noise_maps)

def enhance_contrast(image):
    lab = cv2.cvtColor(image, cv2.COLOR_RGB2LAB)
    l, a, b = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
    cl = clahe.apply(l)
    limg = cv2.merge((cl, a, b))
    enhanced_image = cv2.cvtColor(limg, cv2.COLOR_LAB2RGB)
    return enhanced_image

# Load the input image
image_path  = 'WhatsApp Image 2024-07-04 at 18.57.53_058781c8.jpg'  # Replace with your image path
image = cv2.imread(image_path)

# Check if the image was loaded successfully
if image is None:
    raise FileNotFoundError(f"Image not found at path: {image_path}")

# Convert the image from BGR to RGB
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Apply the filters to the image with amplification
amplification_factor = 4  # Increase this factor to amplify the noise response
noise_maps = apply_filters(image, filters_3_channel, amplification_factor)

# Combine the noise maps to visualize the noise residual
combined_noise_map = np.sum(noise_maps, axis=0)

# Enhance the contrast of the noise residual image
enhanced_noise_map = enhance_contrast(combined_noise_map.astype(np.uint8))

# Plot the original image and the noise residual image
plt.figure(figsize=(10, 5))

plt.subplot(1, 2, 1)
plt.imshow(image)
plt.title('Original Image')
plt.axis('off')

plt.subplot(1, 2, 2)
plt.imshow(enhanced_noise_map)
plt.title('Enhanced Noise Residual Image')
plt.axis('off')

plt.show()
