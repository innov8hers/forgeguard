from flask import Flask, render_template, request
from PIL import Image
from PIL.ExifTags import TAGS

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def upload_image():
    if request.method == 'POST':
        file = request.files['image']
        image_path = './static/' + file.filename
        file.save(image_path)
        
        exif_data = get_exif_data(image_path)
        
        return render_template('index.html', exif_data=exif_data)
    return render_template('index.html')

def get_exif_data(image_path):
    image = Image.open(image_path)
    exif_data = image._getexif()
    
    if exif_data is not None:
        exif = {}
        for tag, value in exif_data.items():
            tag_name = TAGS.get(tag, tag)
            exif[tag_name] = value
        return exif
    else:
        return None

if __name__ == '__main__':
    app.run(debug=True)

